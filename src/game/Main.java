package game;

import java.io.File;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

public class Main extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("game.fxml"));
		Parent root = loader.load();
		primaryStage.setTitle("CarMan - a new version of PacMan");
		Controller controller = loader.getController();
		root.setOnKeyPressed(controller);
		double sceneWidth = controller.mazeWidth() + 200.0;
		double sceneHeight = controller.mazeHeight() + 200.0;
		primaryStage.setScene(new Scene(root, sceneWidth, sceneHeight));
		primaryStage.show();
		root.requestFocus();
		String musicFile = "src/resources/music/music_beginning.wav"; 
		Media sound = new Media(new File(musicFile).toURI().toString());
		MediaPlayer mediaPlayer = new MediaPlayer(sound);
		mediaPlayer.play();

	}

	public static void main(String[] args) {
		launch(args);
	}
}
