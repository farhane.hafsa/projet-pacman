package game;

import javafx.fxml.FXML;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.application.Platform;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class Controller implements EventHandler<KeyEvent> {

	private String musicFileEat = "src/resources/music/music-eat.wav";
	private Media soundEat = new Media(new File(musicFileEat).toURI().toString());

	private String musicFileDeath = "src/resources/music/music_death.wav";
	private Media soundDeath = new Media(new File(musicFileDeath).toURI().toString());

	final private static double FRAMES_PER_SECOND = 5.0;

	private Model model;

	private Timer timer;

	private static int counter;
	
	private static int counterSP;

	private boolean paused;

	@FXML
	private Label score;

	@FXML
	private Label level;

	@FXML
	private Label gameFiniched;

	@FXML
	private View view;

	private static final String[] levels = { "src/resources/levels/level1.txt", "src/resources/levels/level2.txt",
			"src/resources/levels/level3.txt" };

	public Controller() {
		this.paused = false;
	}

	public void initialize() {
		String file = this.getLevelFile(0);
		this.model = new Model();
		this.update(Direction.NONE);
		counter = 30;
		counterSP = 25;
		this.startTimer();
	}

	public double mazeWidth() {
		return View.SIZE * this.view.getColumnCount();
	}

	public double mazeHeight() {
		return View.SIZE * this.view.getRowCount();
	}

	private void startTimer() {
		this.timer = new java.util.Timer();
		TimerTask timerTask = new TimerTask() {
			public void run() {
				Platform.runLater(new Runnable() {
					public void run() {
						update(model.getCurrentDirection());
					}
				});
			}
		};

		long frameTimeInMilliseconds = (long) (1000.0 / FRAMES_PER_SECOND);
		this.timer.schedule(timerTask, 0, frameTimeInMilliseconds);
	}

	private void update(Direction direction) {
		this.model.update(direction);
		this.view.update(model);
		this.score.setText(String.format("SCORE: %d", this.model.getScore()));
		this.level.setText(String.format("LEVEL: %d", this.model.getLevel()));
		if (model.isGameOver()) {
			this.gameFiniched.setText(String.format("GAME OVER"));
			MediaPlayer mediaPlayerDeath = new MediaPlayer(soundDeath);
			mediaPlayerDeath.play();
			pause();
		}
		if (model.isWinner()) {
			this.gameFiniched.setText(String.format("CONGRATULATIONS, YOU WON!"));
		}
		if (model.isPoliceManMad()) {
				counter--;
		}
		if(model.isSuperPower()) {
			counterSP--;
		}
		if(counterSP == 0 && model.isSuperPower()) {
			this.model.setSuperPower(false);
		}
		if(counter ==0 && model.isPoliceManMad()) {
			this.model.setPoliceManMad(false);
			System.out.println("finish");
		}
		this.view.update(model);	
	}

	@Override
	public void handle(KeyEvent keyEvent) {
		boolean keyRecognized = true;
		KeyCode code = keyEvent.getCode();
		Direction direction = Direction.NONE;
		switch (code) {
		case LEFT:
			direction = Direction.LEFT;
			MediaPlayer mediaPlayerEat1 = new MediaPlayer(soundEat);
			mediaPlayerEat1.play();
			break;
		case RIGHT:
			direction = Direction.RIGHT;
			MediaPlayer mediaPlayerEat2 = new MediaPlayer(soundEat);
			mediaPlayerEat2.play();
			break;
		case UP:
			direction = Direction.UP;
			MediaPlayer mediaPlayerEat3 = new MediaPlayer(soundEat);
			mediaPlayerEat3.play();
			break;
		case DOWN:
			direction = Direction.DOWN;
			MediaPlayer mediaPlayerEat4 = new MediaPlayer(soundEat);
			mediaPlayerEat4.play();
			break;
		case ENTER:
			pause();
			this.model.startNewGame();
			this.gameFiniched.setText(String.format(""));
			paused = false;
			this.startTimer();
			break;
		default:
			keyRecognized = false;
		}
		if (keyRecognized) {
			keyEvent.consume();
			model.setCurrentDirection(direction);
		}
	}

	public void pause() {
		this.timer.cancel();
		this.paused = true;
	}

	public static void setCounter() {
		counter = 30;
	}

	public static int getCounter() {
		return counter;
	}
	
	public static void setCounterSP() {
		counterSP = 25;
	}

	public static int getCounterSP() {
		return counterSP;
	}

	public static String getLevelFile(int x) {
		return levels[x];
	}

	public boolean getPaused() {
		return paused;
	}
}
