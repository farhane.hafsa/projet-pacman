package game;

import java.io.File;

import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class View extends Group {

	public final static double SIZE = 25.0;

	@FXML
	private int nb_rows;
	@FXML
	private int nb_columns;

	// pictures of caracters
	private ImageView[][] position;

	// pictures of car directions
	private Image carMovingRight;
	private Image carMovingUp;
	private Image carMovingDown;
	private Image carMovingLeft;

	// pictures of car in super power mode
	private Image carSP;
	private Image superPower;

	// pictures of all of police man types
	private Image policeMan1;
	private Image policeMan2;
	private Image policeManMad;

	// picture of maze's background
	private Image maze;

	// picture of petrol cans
	private Image petrolCanL;
	private Image petrolCanS;

	public View() {
		this.carMovingRight = new Image(getClass().getResourceAsStream("/resources/pictures/carright.png"));
		this.carMovingUp = new Image(getClass().getResourceAsStream("/resources/pictures/carup.png"));
		this.carMovingDown = new Image(getClass().getResourceAsStream("/resources/pictures/cardown.png"));
		this.carMovingLeft = new Image(getClass().getResourceAsStream("/resources/pictures/carleft.png"));
		
		this.carSP = new Image(getClass().getResourceAsStream("/resources/pictures/carSP.png"));
		this.superPower = new Image(getClass().getResourceAsStream("/resources/pictures/superpower.png"));

		this.policeMan1 = new Image(getClass().getResourceAsStream("/resources/pictures/police1.gif"));
		this.policeMan2 = new Image(getClass().getResourceAsStream("/resources/pictures/police1.gif"));
		this.policeManMad = new Image(getClass().getResourceAsStream("/resources/pictures/policeMad.gif"));
		
		this.maze = new Image(getClass().getResourceAsStream("/resources/pictures/maze.png"));
		
		this.petrolCanL = new Image(getClass().getResourceAsStream("/resources/pictures/essenceL.png"));
		this.petrolCanS = new Image(getClass().getResourceAsStream("/resources/pictures/essenceS.png"));
	}

	private void initializeGrid() {

		if (this.nb_rows > 0 && this.nb_columns > 0) {

			this.position = new ImageView[this.nb_rows][this.nb_columns];

			for (int i = 0; i < this.nb_rows; i++) {
				for (int j = 0; j < this.nb_columns; j++) {

					ImageView imageView = new ImageView();

					imageView.setX((double) j * SIZE);
					imageView.setY((double) i * SIZE);

					imageView.setFitWidth(SIZE);
					imageView.setFitHeight(SIZE);

					this.position[i][j] = imageView;
					this.getChildren().add(imageView);
				}
			}
		}
	}

	public void update(Model model) {
		assert model.getRowCount() == this.nb_rows && model.getColumnCount() == this.nb_columns;

		// update the image of each case
		for (int i = 0; i < this.nb_rows; i++) {
			for (int j = 0; j < this.nb_columns; j++) {

				PositionValue value = model.getPositionValue(i, j);
				switch (value) {
				case MAZE:
					this.position[i][j].setImage(this.maze);
					break;
				case ESSENCE_L:
					this.position[i][j].setImage(this.petrolCanL);
					break;
				case ESSENCE_S:
					this.position[i][j].setImage(this.petrolCanS);
					break;
				case SUPER_POWER:
					this.position[i][j].setImage(this.superPower);
					break;
				default:
					this.position[i][j].setImage(null);
				}

				// update the direction of the car
				if (i == model.getCarLocation().getX() && j == model.getCarLocation().getY()
						&& (Model.getLastDirection() == Direction.RIGHT
								|| Model.getLastDirection() == Direction.NONE)) {
					if (Model.isPoliceManMad()) {
						this.position[i][j].setImage(this.carSP);
					} else {
						this.position[i][j].setImage(this.carMovingRight);
					}

				} else if (i == model.getCarLocation().getX() && j == model.getCarLocation().getY()
						&& Model.getLastDirection() == Direction.LEFT) {
					if (Model.isPoliceManMad()) {
						this.position[i][j].setImage(this.carSP);
					} else {
						this.position[i][j].setImage(this.carMovingLeft);
					}

				} else if (i == model.getCarLocation().getX() && j == model.getCarLocation().getY()
						&& Model.getLastDirection() == Direction.UP) {
					if (Model.isPoliceManMad()) {
						this.position[i][j].setImage(this.carSP);
					} else {
						this.position[i][j].setImage(this.carMovingUp);
					}

				} else if (i == model.getCarLocation().getX() && j == model.getCarLocation().getY()
						&& Model.getLastDirection() == Direction.DOWN) {
					if (Model.isPoliceManMad()) {
						this.position[i][j].setImage(this.carSP);
					} else {
						this.position[i][j].setImage(this.carMovingDown);
					}
				}

				if (Model.isPoliceManMad() && (Controller.getCounter() == 6 || Controller.getCounter() == 4
						|| Controller.getCounter() == 2)) {
					if (i == model.getPoliceMan1Location().getX() && j == model.getPoliceMan1Location().getY()) {
						this.position[i][j].setImage(this.policeMan1);
					}
					if (i == model.getPoliceMan2Location().getX() && j == model.getPoliceMan2Location().getY()) {
						this.position[i][j].setImage(this.policeMan2);
					}
				}
				// mad cops
				else if (Model.isPoliceManMad()) {
					if (i == model.getPoliceMan1Location().getX() && j == model.getPoliceMan1Location().getY()) {
						this.position[i][j].setImage(this.policeManMad);
					}
					if (i == model.getPoliceMan2Location().getX() && j == model.getPoliceMan2Location().getY()) {
						this.position[i][j].setImage(this.policeManMad);
					}
				}
				// regular mode
				else {
					if (i == model.getPoliceMan1Location().getX() && j == model.getPoliceMan1Location().getY()) {
						this.position[i][j].setImage(this.policeMan1);
					}
					if (i == model.getPoliceMan2Location().getX() && j == model.getPoliceMan2Location().getY()) {
						this.position[i][j].setImage(this.policeMan2);
					}
				}
			}
		}
	}

	public int getRowCount() {
		return this.nb_rows;
	}

	public void setRowCount(int rowCount) {
		this.nb_rows = rowCount;
		this.initializeGrid();
	}

	public int getColumnCount() {
		return this.nb_columns;
	}

	public void setColumnCount(int columnCount) {
		this.nb_columns = columnCount;
		this.initializeGrid();
	}
}
