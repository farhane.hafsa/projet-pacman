package game;

import javafx.geometry.Point2D;

import javafx.fxml.FXML;
import java.io.*;

import java.util.*;

public class Model {

	@FXML
	private int nb_rows;

	@FXML
	private int nb_columns;

	private PositionValue[][] grid;

	private int score;
	private int level;
	private int essence;

	private static boolean gameOver;
	private static boolean winner;
	private static boolean policeManMad;
	private static boolean superPower;

	private Point2D carLocation;
	private Point2D carSpeed;
	private Point2D policeMan1Location;
	private Point2D policeMan1Speed;
	private Point2D policeMan2Location;
	private Point2D policeMan2Speed;

	private static Direction lastDirection;
	private static Direction currentDirection;

	public Model() {
		this.startNewGame();
	}

	public void initializeLevel(String fileName) {
		File file = new File(fileName);
		Scanner scanner = null;
		try {
			scanner = new Scanner(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			Scanner lineScanner = new Scanner(line);
			while (lineScanner.hasNext()) {
				lineScanner.next();
				nb_columns++;
			}
			nb_rows++;
		}
		nb_columns = nb_columns / nb_rows;
		Scanner scanner2 = null;
		try {
			scanner2 = new Scanner(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		grid = new PositionValue[nb_rows][nb_columns];
		int row = 0, carRow = 0, carColumn = 0, policeMan1Row = 0, policeMan1Column = 0, policeMan2Row = 0,
				policeMan2Column = 0;
		while (scanner2.hasNextLine()) {
			int column = 0;
			// read the levels'file
			String line = scanner2.nextLine();
			Scanner lineScanner = new Scanner(line);

			while (lineScanner.hasNext()) {
				String value = lineScanner.next();
				PositionValue thisValue;
				switch (value) {
				case "M":
					thisValue = PositionValue.MAZE;
					break;
				case "S":
					thisValue = PositionValue.ESSENCE_S;
					essence++;
					break;
				case "L":
					thisValue = PositionValue.ESSENCE_L;
					essence++;
					break;
				case "P":
					thisValue = PositionValue.SUPER_POWER;
					essence++;
					break;
				case "1":
					thisValue = PositionValue.POLICEMAN1;
					policeMan1Row = row;
					policeMan1Column = column;
					break;
				case "2":
					thisValue = PositionValue.POLICEMAN2;
					policeMan2Row = row;
					policeMan2Column = column;
					break;
				case "C":
					thisValue = PositionValue.CAR;
					carRow = row;
					carColumn = column;
					break;
				default:
					thisValue = PositionValue.EMPTY;
				}
				grid[row][column] = thisValue;
				column++;
			}
			row++;
		}
		carLocation = new Point2D(carRow, carColumn);
		carSpeed = new Point2D(0, 0);
		policeMan1Location = new Point2D(policeMan1Row, policeMan1Column);
		policeMan1Speed = new Point2D(-1, 0);
		policeMan2Location = new Point2D(policeMan2Row, policeMan2Column);
		policeMan2Speed = new Point2D(-1, 0);
		currentDirection = Direction.NONE;
		lastDirection = Direction.NONE;
	}

	public void startNewGame() {
		this.gameOver = false;
		this.winner = false;
		this.policeManMad = false;
		this.superPower = false;
		essence = 0;
		nb_rows = nb_columns = 0;
		this.score = 0;
		this.level = 1;
		this.initializeLevel(Controller.getLevelFile(0));
	}

	public void startNextLevel() {
		if (this.isLevelComplete()) {
			this.level++;
			nb_rows = 0;
			nb_columns = 0;
			winner = false;
			policeManMad = false;
			superPower = false;
			try {
				this.initializeLevel(Controller.getLevelFile(level - 1));
			} catch (ArrayIndexOutOfBoundsException e) {
				// if there are no levels left in the level array, the game ends
				winner = true;
				gameOver = true;
				level--;
			}
		}
	}

	public Point2D changeSpeed(Direction direction) {
		Point2D point2d;
		switch (direction) {
		case LEFT:
			point2d = new Point2D(0, -1);
			break;
		case RIGHT:
			point2d = new Point2D(0, 1);
			break;
		case UP:
			point2d = new Point2D(-1, 0);
			break;
		case DOWN:
			point2d = new Point2D(1, 0);
			break;
		default:
			point2d = new Point2D(0, 0);

		}
		return point2d;
	}

	public void moveCar(Direction direction) {
		Point2D potentialcarSpeed = changeSpeed(direction);
		Point2D potentialCarLocation = carLocation.add(potentialcarSpeed);
		potentialCarLocation = setGoingOffscreenNewLocation(potentialCarLocation);

		if (direction.equals(lastDirection)) {
			if (grid[(int) potentialCarLocation.getX()][(int) potentialCarLocation.getY()] == PositionValue.MAZE) {
				carSpeed = changeSpeed(Direction.NONE);
				setLastDirection(Direction.NONE);
			} else {
				carSpeed = potentialcarSpeed;
				carLocation = potentialCarLocation;
			}
		} else {
			if (grid[(int) potentialCarLocation.getX()][(int) potentialCarLocation.getY()] == PositionValue.MAZE) {
				if(superPower) {
					System.out.println("super power");
					carSpeed = potentialcarSpeed;
					carLocation = potentialCarLocation;
					setLastDirection(direction);
				}
				else {
					potentialcarSpeed = changeSpeed(lastDirection);
					potentialCarLocation = carLocation.add(potentialcarSpeed);
					if (grid[(int) potentialCarLocation.getX()][(int) potentialCarLocation.getY()] == PositionValue.MAZE) {
						carSpeed = changeSpeed(Direction.NONE);
						setLastDirection(Direction.NONE);
					} else {
						carSpeed = changeSpeed(lastDirection);
						carLocation = carLocation.add(carSpeed);
					}
				}
				
			} else {
				carSpeed = potentialcarSpeed;
				carLocation = potentialCarLocation;
				setLastDirection(direction);
			}
		}
	}

	// make the cops follow the car
	public Point2D[] moveAPoliceMan(Point2D speed, Point2D location) {
		Random generator = new Random();
		if (!policeManMad) {
			if (location.getY() == carLocation.getY()) {
				if (location.getX() > carLocation.getX()) {
					speed = changeSpeed(Direction.UP);
				} else {
					speed = changeSpeed(Direction.DOWN);
				}
				Point2D potentialLocation = location.add(speed);
				potentialLocation = setGoingOffscreenNewLocation(potentialLocation);
				while (grid[(int) potentialLocation.getX()][(int) potentialLocation.getY()] == PositionValue.MAZE) {
					int randomNum = generator.nextInt(4);
					Direction direction = intToDirection(randomNum);
					speed = changeSpeed(direction);
					potentialLocation = location.add(speed);
				}
				location = potentialLocation;
			} else if (location.getX() == carLocation.getX()) {
				if (location.getY() > carLocation.getY()) {
					speed = changeSpeed(Direction.LEFT);
				} else {
					speed = changeSpeed(Direction.RIGHT);
				}
				Point2D potentialLocation = location.add(speed);
				potentialLocation = setGoingOffscreenNewLocation(potentialLocation);
				while (grid[(int) potentialLocation.getX()][(int) potentialLocation.getY()] == PositionValue.MAZE) {
					int randomNum = generator.nextInt(4);
					Direction direction = intToDirection(randomNum);
					speed = changeSpeed(direction);
					potentialLocation = location.add(speed);
				}
				location = potentialLocation;
			} else {
				Point2D potentialLocation = location.add(speed);
				potentialLocation = setGoingOffscreenNewLocation(potentialLocation);
				while (grid[(int) potentialLocation.getX()][(int) potentialLocation.getY()] == PositionValue.MAZE) {
					int randomNum = generator.nextInt(4);
					Direction direction = intToDirection(randomNum);
					speed = changeSpeed(direction);
					potentialLocation = location.add(speed);
				}
				location = potentialLocation;
			}
		}
		// if police Man is Mad, he should run from the car
		if (policeManMad) {
			if (location.getY() == carLocation.getY()) {
				if (location.getX() > carLocation.getX()) {
					speed = changeSpeed(Direction.DOWN);
				} else {
					speed = changeSpeed(Direction.UP);
				}
				Point2D potentialLocation = location.add(speed);
				potentialLocation = setGoingOffscreenNewLocation(potentialLocation);
				while (grid[(int) potentialLocation.getX()][(int) potentialLocation.getY()] == PositionValue.MAZE) {
					int randomNum = generator.nextInt(4);
					Direction direction = intToDirection(randomNum);
					speed = changeSpeed(direction);
					potentialLocation = location.add(speed);
				}
				location = potentialLocation;
			} else if (location.getX() == carLocation.getX()) {
				if (location.getY() > carLocation.getY()) {
					speed = changeSpeed(Direction.RIGHT);
				} else {
					speed = changeSpeed(Direction.LEFT);
				}
				Point2D potentialLocation = location.add(speed);
				potentialLocation = setGoingOffscreenNewLocation(potentialLocation);
				while (grid[(int) potentialLocation.getX()][(int) potentialLocation.getY()] == PositionValue.MAZE) {
					int randomNum = generator.nextInt(4);
					Direction direction = intToDirection(randomNum);
					speed = changeSpeed(direction);
					potentialLocation = location.add(speed);
				}
				location = potentialLocation;
			} else {
				Point2D potentialLocation = location.add(speed);
				potentialLocation = setGoingOffscreenNewLocation(potentialLocation);
				while (grid[(int) potentialLocation.getX()][(int) potentialLocation.getY()] == PositionValue.MAZE) {
					int randomNum = generator.nextInt(4);
					Direction direction = intToDirection(randomNum);
					speed = changeSpeed(direction);
					potentialLocation = location.add(speed);
				}
				location = potentialLocation;
			}
		}
		Point2D[] data = { speed, location };
		return data;

	}

	public void movePolices() {
		Point2D[] policeMan1Data = moveAPoliceMan(policeMan1Speed, policeMan1Location);
		Point2D[] policeMan2Data = moveAPoliceMan(policeMan2Speed, policeMan2Location);
		policeMan1Speed = policeMan1Data[0];
		policeMan1Location = policeMan1Data[1];
		policeMan2Speed = policeMan2Data[0];
		policeMan2Location = policeMan2Data[1];

	}

	public Direction intToDirection(int x) {
		Direction direction;
		switch (x) {
		case 0:
			direction = Direction.LEFT;
			break;
		case 1:
			direction = Direction.RIGHT;
			break;
		case 2:
			direction = Direction.UP;
			break;
		default:
			direction = Direction.DOWN;
		}
		return direction;
	}

	public void policeMan1Reset() {
		for (int i = 0; i < this.nb_rows; i++) {
			for (int j = 0; j < this.nb_columns; j++) {
				if (grid[i][j] == PositionValue.POLICEMAN1) {
					policeMan1Location = new Point2D(i, j);
				}
			}
		}
		policeMan1Speed = new Point2D(-1, 0);
	}

	public void policeMan2Reset() {
		for (int i = 0; i < this.nb_rows; i++) {
			for (int j = 0; j < this.nb_columns; j++) {
				if (grid[i][j] == PositionValue.POLICEMAN2) {
					policeMan2Location = new Point2D(i, j);
				}
			}
		}
		policeMan2Speed = new Point2D(-1, 0);
	}

	public void update(Direction direction) {
		this.moveCar(direction);
		PositionValue carLocationPositionValue = grid[(int) carLocation.getX()][(int) carLocation.getY()];
		switch (carLocationPositionValue) {
		case ESSENCE_S:
			grid[(int) carLocation.getX()][(int) carLocation.getY()] = PositionValue.EMPTY;
			essence--;
			score += 10;
			break;
		case ESSENCE_L:
			grid[(int) carLocation.getX()][(int) carLocation.getY()] = PositionValue.EMPTY;
			essence--;
			score += 50;
			policeManMad = true;
			Controller.setCounter();
		case SUPER_POWER:
			grid[(int) carLocation.getX()][(int) carLocation.getY()] = PositionValue.EMPTY;
			superPower = true;
		}
		if (policeManMad) {
			if (carLocation.equals(policeMan1Location)) {
				policeMan1Reset();
				score += 100;
			}
			if (carLocation.equals(policeMan2Location)) {
				policeMan2Reset();
				score += 100;
			}
		} else {
			if (carLocation.equals(policeMan1Location)) {
				gameOver = true;
				carSpeed = new Point2D(0, 0);
			}
			if (carLocation.equals(policeMan2Location)) {
				gameOver = true;
				carSpeed = new Point2D(0, 0);
			}
		}
		this.movePolices();
		if (policeManMad) {
			if (carLocation.equals(policeMan1Location)) {
				policeMan1Reset();
				score += 100;
			}
			if (carLocation.equals(policeMan2Location)) {
				policeMan2Reset();
				score += 100;
			}
		} else {
			if (carLocation.equals(policeMan1Location)) {
				gameOver = true;
				carSpeed = new Point2D(0, 0);
			}
			if (carLocation.equals(policeMan2Location)) {
				gameOver = true;
				carSpeed = new Point2D(0, 0);
			}
		}
		if (this.isLevelComplete()) {
			carSpeed = new Point2D(0, 0);
			startNextLevel();
		}
	}

	public Point2D setGoingOffscreenNewLocation(Point2D objectLocation) {
		if (objectLocation.getY() >= nb_columns) {
			objectLocation = new Point2D(objectLocation.getX(), 0);
		}
		if (objectLocation.getY() < 0) {
			objectLocation = new Point2D(objectLocation.getX(), nb_columns - 1);
		}
		return objectLocation;
	}

	public static boolean isPoliceManMad() {
		return policeManMad;
	}

	public static void setPoliceManMad(boolean b) {
		policeManMad = b;
	}

	public static boolean isWinner() {
		return winner;
	}

	public boolean isLevelComplete() {
		return this.essence == 0;
	}

	public static boolean isGameOver() {
		return gameOver;
	}

	public PositionValue[][] getGrid() {
		return grid;
	}

	public PositionValue getPositionValue(int row, int column) {
		assert row >= 0 && row < this.grid.length && column >= 0 && column < this.grid[0].length;
		return this.grid[row][column];
	}

	public static Direction getCurrentDirection() {
		return currentDirection;
	}

	public void setCurrentDirection(Direction direction) {
		currentDirection = direction;
	}

	public static Direction getLastDirection() {
		return lastDirection;
	}

	public void setLastDirection(Direction direction) {
		lastDirection = direction;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public void incrementeScore(int points) {
		this.score += points;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getEssenceCount() {
		return essence;
	}

	public void setEssencCount(int dotCount) {
		this.essence = dotCount;
	}

	public int getRowCount() {
		return nb_rows;
	}

	public void setRowCount(int rowCount) {
		this.nb_rows = rowCount;
	}

	public int getColumnCount() {
		return nb_columns;
	}

	public void setColumnCount(int columnCount) {
		this.nb_columns = columnCount;
	}

	public Point2D getCarLocation() {
		return carLocation;
	}

	public void setCarLocation(Point2D pacmanLocation) {
		this.carLocation = pacmanLocation;
	}

	public Point2D getPoliceMan1Location() {
		return policeMan1Location;
	}

	public void setPoliceMan1Location(Point2D policeMan1Location) {
		this.policeMan1Location = policeMan1Location;
	}

	public Point2D getPoliceMan2Location() {
		return policeMan2Location;
	}

	public void setPoliceMan2Location(Point2D ghost2Location) {
		this.policeMan2Location = ghost2Location;
	}

	public Point2D getcarSpeed() {
		return carSpeed;
	}

	public void setcarSpeed(Point2D velocity) {
		this.carSpeed = velocity;
	}

	public Point2D getPoliceMan1Speed() {
		return policeMan1Speed;
	}

	public void setPoliceMan1Speed(Point2D policeMan1Speed) {
		this.policeMan1Speed = policeMan1Speed;
	}

	public Point2D getPoliceMan2Speed() {
		return policeMan2Speed;
	}

	public void setPoliceMan2Speed(Point2D policeMan2Speed) {
		this.policeMan2Speed = policeMan2Speed;
	}

	public static boolean isSuperPower() {
		return superPower;
	}

	public void setSuperPower(boolean b) {
		this.superPower = b;
	}
}
