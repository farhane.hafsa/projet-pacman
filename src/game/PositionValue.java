package game;

public enum PositionValue {
	EMPTY, 
	ESSENCE_S, 
	ESSENCE_L, 
	MAZE, 
	POLICEMAN1, 
	POLICEMAN2, 
	CAR,
	SUPER_POWER
}
