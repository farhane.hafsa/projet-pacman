package game;

public class Police {

	private int idPoliceMan;

	private boolean isMad;

	public Police(int idPoliceMan, boolean isMad) {
		super();
		this.idPoliceMan = idPoliceMan;
		this.isMad = isMad;
	}

	public int getIdPoliceMan() {
		return idPoliceMan;
	}

	public void setIdPoliceMan(int idPoliceMan) {
		this.idPoliceMan = idPoliceMan;
	}

	public boolean isMad() {
		return isMad;
	}

	public void setMad(boolean isMad) {
		this.isMad = isMad;
	}

}
